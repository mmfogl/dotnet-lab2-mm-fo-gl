﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;



namespace gameoflifeconsole
{
    
    class Komorki
    {
        public char[,] komorki_teraz = new char[20, 20];
        public char[,] komorki_wczesniej = new char[20, 20];

        public void kopia()
        {
            for (int i = 0; i < 20; ++i)
            {
                for (int j = 0; j < 20; ++j)
                {
                    if (komorki_teraz[i, j] == 'X')
                    {
                        komorki_wczesniej[i, j] = 'X';
                    }
                    else
                    {
                        komorki_wczesniej[i, j] = '.';
                    }
                }
            }
        }


        
        public void init(int x)
        {
            Random rnd1 = new Random();
            for (int w = 0; w < 20; ++w)
            {
                for (int k = 0; k < 20; ++k)
                {
                    komorki_teraz[w, k] = '.';
                }
            }
            
            for (int i = 0; i < x; ++i)
            {
                komorki_teraz[rnd1.Next(0, 20), rnd1.Next(0, 20)] = 'X';
            }
             
            /*
            komorki_teraz[10, 9] = 'X';
            komorki_teraz[10, 10] = 'X';
            komorki_teraz[10, 11] = 'X';
             * */
        }

        public void wypis()
        {
            Console.Clear();
            for (int w = 0; w < 20; ++w)
            {
                for (int k = 0; k < 20; ++k)
                {
                    Console.Write(komorki_teraz[w, k]);

                }
                Console.WriteLine();
            }



        }

    }
    class Program
    {



        static void Main(string[] args)
        {
            Komorki plansza = new Komorki();
            Console.WriteLine("podaj maksymalna ilosc losowych komorek do rozmieszczenia na panszy");
            int ilosc = Convert.ToInt32(Console.ReadLine());
            plansza.init(ilosc);
            //plansza.komorki_teraz[10, 10] = 'X';

            plansza.wypis();
            Console.WriteLine("Wciśnij przycisk by kontynuować");
            Console.ReadKey();
            int iter = 0;
            while (true)
            {
                //char[,] komorki_wczesniej = new char[10, 10];
                plansza.kopia();
                iter++;
                for (int z = 0; z < 20; ++z)
                {
                    for (int c = 0; c < 20; ++c)
                    {
                        int sasiedzi = 0;
                        for (int i = -1; i <= 1; i++)
                        {
                            for (int k = -1; k <= 1; k++)
                            {
                                if (z + i > 0 && z + i < 20)
                                {
                                    if (c + k > 0 && c + k < 20)
                                    {
                                        if (plansza.komorki_wczesniej[z + i, c + k] == 'X') sasiedzi++;
                                                                         
                                    }
                                
                                }
                            }
                        }
                        if (plansza.komorki_wczesniej[z, c] == 'X') sasiedzi--;

                        if ((sasiedzi ==2) || (sasiedzi ==3))
                        {
                            if (plansza.komorki_wczesniej[z, c] == 'X')
                            {
                                  plansza.komorki_teraz[z, c] = 'X';
                            }

                            if ((plansza.komorki_wczesniej[z, c] == '.'))
                            {
                                if ((sasiedzi == 3))
                                {
                                    plansza.komorki_teraz[z, c] = 'X';
                                }
                                else
                                {
                                    plansza.komorki_teraz[z, c] = '.';
                                }
                            }

                        }
                        else
                        {
                            plansza.komorki_teraz[z, c] = '.';
                        }

                    }
                }
                plansza.wypis();
                Console.WriteLine("Chwila: " + iter.ToString());
                Thread.Sleep(1000);
            }
        }



    }
}
